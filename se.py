class Settings:
    def __init__(self, start, end, delimitors, type_):
        
        self.start = start
        self.end = end
        self.delimitors = tuple(delimitors)
        self.type = type_

class Variable:
    def __init__(self, string, settings):
        self.s = settings
        strlist = string.split(self.s.delimitors[0])
        keys = [strlist[n] for n in range(0,len(strlist),2)]
        values = [strlist[n] for n in range(1,len(strlist),2)]
        self.d = dict()
        for key in ["range","set"]:
            if key in keys:
                self.d[key] = values[keys.index(key)].split(self.s.delimitors[1])
            else:
                self.d[key] = None
        if self.d["range"]:
            self.chars = "".join([chr(i) for i in range(ord(self.d["range"][0]),ord(self.d["range"][1])+1)])
        else:
            self.chars = self.d["set"]

    def __repr__(self):
        return f"Variable(chars='{self.chars}')"

class Constant:
    def __init__(self, string, settings):
        self.s = settings
        strlist = string.split(self.s.delimitors[0]) if not "" in string.split(self.s.delimitors[0]) else [" "]
        self.values = tuple([i.replace(r"\s"," ") for i in strlist])
        
    def __repr__(self):
        v = ' '.join([v.replace("\n","\\n").replace(" ",r"\s") for v in self.values])
        return f"Constant(chars='{v}')"
    
class Finder:
    def __init__(self, settings_v = None, settings_c = None):
        if not settings_v:
            self.s_v = Settings("v(",")",[" ", ","], Variable)
        else:
            self.s_v = settings_v
        if not settings_c:
            self.s_c = Settings("c(",")",[" ", ","], Constant)
        else:
            self.s_c = settings_c

    def __call__(self, string, pattern):
        return self.get_indexes(string, pattern)

    def get_matches(self, string, pattern):
        return [string[i[0]:i[1]] for i in self.get_indexes(string, pattern)]

    def get_indexes(self, string, pattern):
        starter = 0
        indexes = []
        while 1:
            s = string[starter:]
            ind = self.get_index(s, pattern, starter)
            if ind==-1:
                break
            else:
                indexes.append(ind)
                starter = ind[-1]
        return indexes

    def get_indexes_lists(self, string, pattern):
        starter = 0
        indexes = []
        starter_ = "<start>"
        ender_ = "<end>"
        string = starter_+string+ender_
        while 1:
            s = string[starter:]
            ind = self.get_index(s, pattern, starter)
            if ind==-1:
                break
            else:
                indexes.append(ind)
                starter = ind[-1]
        indexes2 = []
        for ind in indexes:
            o,t = [i-len(starter_) for i in ind]
            if t>len(string)-len(starter_)-len(ender_):
                t = len(string)-len(starter_)-len(ender_)
            if o<0:
                o = 0
            indexes2.append([o,t])
        return indexes2

    def get_index(self, string, pattern, addition = 0):
        objects = self.get_objects(pattern)
        
        if self.match_constants(string, objects):
            gaps = self.find_gaps(string, objects)
            strlist = [string]
            for gap in gaps:
                strlist[-1:] = strlist[-1].split(gap,1)
            variables = strlist[1:-1]
            var_temps = [o for o in objects if type(o)==Variable]
            if len(variables)!=len(var_temps) or False in [self.verify_variable(variables[n],var_temps[n]) for n in range(len(variables))]:
                ind = -1
            else:
                ind = (string.find(gaps[0])+addition, self.find_last(string, objects)+addition)
            return ind
        else:
            return -1

    def verify_variable(self, var, template):
        valid = True
        if template.chars:
            if not set(var).issubset(set(template.chars)):
                valid = False
        return valid

    def find_gaps(self, string, objects):
        constants = [o for o in objects if type(o)==Constant]
        focus = 0
        gaps = []
        for con in constants:
            found = False
            clip = sorted([(string.find(val,focus), val) for val in con.values if string.find(val,focus)!=-1])
            values = [i[1] for i in clip]
            for val in values:
                if string.find(val,focus)!=-1:
                    gaps.append(val)
                    focus = string.find(val,focus)+len(val)
                    found = True
                    break
            if found:
                continue
            else:
                focus = -1
                break
        return gaps
    
    def match_constants(self, string, objects):
        constants = [o for o in objects if type(o)==Constant]
        focus = 0
        for con in constants:
            found = False
            clip = sorted([(string.find(val,focus), val) for val in con.values if string.find(val,focus)!=-1])
            values = [i[1] for i in clip]
            for val in values:
                if string.find(val,focus)!=-1:
                    focus = string.find(val,focus)+len(val)
                    found = True
                    break
            if found:
                continue
            else:
                focus = -1
                break
        return focus!=-1

    def find_last(self, string, objects):
        constants = [o for o in objects if type(o)==Constant]
        focus = 0
        for con in constants:
            found = False
            clip = sorted([(string.find(val,focus), val) for val in con.values if string.find(val,focus)!=-1])
            values = [i[1] for i in clip]
            for val in values:
                if string.find(val,focus)!=-1:
                    focus = string.find(val,focus)+len(val)
                    found = True
                    break
            if found:
                continue
            else:
                focus = -1
                break
        return focus
                
    def get_objects_old(self, string):
        n = 0
        objects = []
        buffer = ""
        while n<len(string):
            is_command = False
            for o in (self.s_v, self.s_c):
                if string[n:].startswith(o.start):
                    objects.append( o.type(string[n+len(o.start):string.find(o.end,n)], o) )
                    is_command = True
            if is_command:
                if buffer:
                    objects.append( Constant(buffer ,self.s_c) )
                    buffer = ""
                n = string.find(o.end,n)+len(o.end)
            else:
                buffer+=string[n]
                #objects.append( Constant(string[n] ,self.s_c) )
                n+=1
        if buffer:
            objects.append( Constant(buffer ,self.s_c) )
            buffer = ""
        print(objects)
        return objects


    def get_objects(self, string):
        n = 0
        objects = [""]
        starters = [o.start for o in (self.s_v, self.s_c)]
        while string:
            if True in [string.startswith(s) for s in starters]:
                if string.startswith(self.s_v.start):
                    text = string[len(self.s_v.start):string.find(self.s_v.end)]
                    objects.append(Variable(text, self.s_v))
                    objects.append("")
                    string = string[string.find(self.s_v.end)+len(self.s_v.end):]
                else:
                    text = string[len(self.s_c.start):string.find(self.s_c.end)]
                    objects.append(Constant(text, self.s_c))
                    objects.append("")
                    string = string[string.find(self.s_c.end)+len(self.s_c.end):]
            else:
                objects[-1]+=string[0]
                string = string[1:]
        objects = [o for o in objects if o]
        objs = []
        
        for o in objects:
            if type(o)==str:
                objs.append(Constant(o, self.s_c))
            else:###
                objs.append(o)
        return objs
            

if __name__=="__main__":
    f = Finder()
    found = f("[sus][amogus]","[v(range a,z)]")
    print(found)
    print(f.get_matches("[sus] [amogus] [123] :pingas: :123:",":v(range a,z):"))
    links = f.get_matches("text https://www.litechan.org/ran/67 more text https://www.amongus.com sus https://www.litechan.org/ran/67\n xddd", "https://v()c(\s \n)")
    print(links)
    print(f.get_indexes_lists("asus","c(<start>)v()c(<end>)"))
