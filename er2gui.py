from er2 import *
import os
from se import Finder
from tkinter import *

class GUI(Tk):
    def __init__(self):
        super().__init__()
        self.title("Eremias 2")
        style = {"bg": "#000000",
                 "fg": "#ffd700",
                 "bd": 0,
                 "highlightthickness": 0,
                 "font": ("Fixedsys", 24),###CHANGE FONT HERE
                 "relief": FLAT}
        self.insertion = ""
        self.history = []
        self.hisfocus = 0
        self.fullscreen = False
        self.var = StringVar()
        self.text = Text(self, width = 0, height = 0, insertbackground = "#ffd700", insertwidth = 8, selectbackground = "#ffd700", **style)
        self.entry = Entry(self, textvariable = self.var, insertbackground = "#ffd700", insertwidth = 8, selectbackground = "#ffd700", **style)
        self.text.pack(fill=BOTH,expand=True)
        self.entry.pack(fill=X)
        self.geometry("500x300")

        self.text.tag_config("pfuns", foreground = "#ff7200")
        self.text.tag_config("funs", foreground = "#0cff00")
        self.text.tag_configure("console", foreground = "#00d8ff")
        self.text.tag_configure("lindex", foreground = "#777777")
        self.text.tag_configure("tindex", foreground = "#daa06d")
        self.finder = Finder()
        self.after(100, self.highlight_manager)
        self.bind("<Escape>", self.toggle_screen)
        self.entry.bind("<Return>", self.io_man)
        self.entry.bind("<Up>", self.up)
        self.entry.bind("<Down>", self.down)
        self.text.config(state=DISABLED)
        self.entry.focus_set()

        self.mainloop()

    def toggle_screen(self, event = None):
        self.fullscreen = not self.fullscreen
        self.attributes("-fullscreen", self.fullscreen)

    def up(self, event = None):
        old = self.hisfocus
        try:
            self.hisfocus-=1
            self.var.set(self.history[self.hisfocus])
            self.entry.icursor(END)
        except Exception:
            self.hisfocus = old

    def down(self, event = None):
        if self.hisfocus==-1:
            self.var.set("")
        else:
            self.hisfocus+=1
            self.var.set(self.history[self.hisfocus])
            self.entry.icursor(END)

    def highlight_manager(self, event = None):
        pfuns = ["goto","warp","print"]
        funs = ["input", "len", "sub", "++", "--", "convert", "append"]
        console = ["run","all","read","edit","save","compile"]
        [self.text.tag_remove(tag, "1.0", END) for tag in ["pfuns","funs","console","index"]]
        
        for pf in pfuns:
            pf = pf+" "
            self.highlight(pf, "pfuns")

        for f in funs:
            if not f in ["++","--"]:
                f = f+" "
            self.highlight(f, "funs")

        for c in console:
            c = c
            self.highlight(c, "console")

        for n, line in enumerate(self.text.get("1.0",END).split("\n")):
            for ind in self.finder.get_indexes_lists(line, '"v()"'):
                self.text.tag_add("lindex", f"{n+1}.{ind[0]}", f"{n+1}.{ind[1]}")
            for ind in self.finder.get_indexes_lists(line, 'c(<start> \n)v(set 0,9,8,7,6,5,4,3,2,1,a,b,c,d,e,f)c(\s .)'):
                self.text.tag_add("tindex", f"{n+1}.{ind[0]}", f"{n+1}.{ind[1]}")
            
        self.after(100, self.highlight_manager)

    def highlight(self, keyword, tag):
        """https://stackoverflow.com/questions/17829713/tkinter-highlight-colour-specific-lines-of-text-based-on-a-keyword"""
        pos = '1.0'
        while True:
            idx = self.text.search(keyword, pos, END)
            if not idx:
                break
            pos = '{}+{}c'.format(idx, len(keyword))
            #print(pos)
            self.text.tag_add(tag, idx, pos)

    def io_man(self, event = None):
        command = self.var.get()
        self.var.set("")
        self.text.config(state=NORMAL)
        self.text.insert(END, "\n"+command)
        self.text.config(state=DISABLED)
        self.hisfocus = 0
        if self.insertion:
            self.variables["__input__out"] = command.replace(self.insertion,"",1)
            self.insertion = ""
        else:
            self.history.append(command)
            if command.startswith("run"):
                fname = command.split(" ",1)[1]
                self.run(fname)
            elif command=="all":
                cwd = os.getcwd()
                results = []
                for path, subdirs, files in os.walk(cwd):
                    for file in files:
                        if file.endswith(".ems") or file.endswith(".cems"):
                            pname = os.path.join(path.replace(cwd,"",1),file)
                            if pname[0] in ("\\","/"):
                                pname = f".{pname}"
                            results.append(pname)
                self.text.config(state=NORMAL)
                self.text.insert(END,"\n"+"\n".join(sorted(results)))
                self.text.config(state=DISABLED)
            elif command.startswith("read"):
                c, fname = command.split(" ",1)
                with open(fname) as f:
                    text = f.read()
                self.text.config(state=NORMAL)
                self.text.insert(END,"\n"+text)
                self.text.config(state=DISABLED)
            elif command.startswith("edit"):
                c, fname = command.split(" ",1)
                with open(fname) as f:
                    text = f.read()
                self.text.config(state=NORMAL)
                self.text.delete("1.0",END)
                self.text.insert(END, text)
                self.text.focus_set()

            elif command=="new" or command=="clear":
                self.text.config(state=NORMAL)
                self.text.delete("1.0", END)
                self.text.focus_set()

            elif command.startswith("save"):
                text = "\n".join( self.text.get("1.0", END).split("\n")[:-2] )
                self.text.config(state=DISABLED)
                with open(command.split(" ",1)[1], "w") as f:
                    f.write(text)

            elif command.startswith("compile"):
                c, fname = command.split(" ",1)
                with open(fname) as f:
                    text = f.read()
                self.text.config(state=NORMAL)
                self.text.insert(END,"\n"+str(Interpreter(text)))
                self.text.config(state=DISABLED)
            self.text.see(END)

    def run(self, fname):
        with open(fname) as f:
            code = f.read()
        self.manager = Manager()
        template = {"__print__": [], "__input__": [], "__warp__": [], "__input__in": "", "__input__out": ""}
        self.variables = self.manager.dict(template)
        self.workers = [Process(target = worker, args = (code, self.variables))]
        self.workers[-1].start()
        while True in [w.is_alive() for w in self.workers]:
            if self.variables.get("__print__"):
                to_print = self.variables.get("__print__")
                print(to_print)
                self.variables["__print__"] = []
                self.text.config(state=NORMAL)
                self.text.insert(END, "\n"+"\n".join(to_print))
                self.text.config(state=DISABLED)
                self.text.see(END)
            if self.variables["__input__in"]:
                if not self.insertion:
                    self.insertion = self.variables["__input__in"]
                    self.var.set(self.insertion)
                    self.entry.icursor(END)
            if self.variables["__warp__"]:
                l = self.variables["__warp__"]
                ind = l.pop(0)
                self.workers.append(Process(target = worker, args = (code, self.variables, ind)))
                self.workers[-1].start()
                self.variables["__warp__"] = l[:]
                
            self.update()
            sleep(0.01)

if __name__=="__main__":
    gui = GUI()
