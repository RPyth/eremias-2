from multiprocessing import Process, Manager
from decimal import Decimal, getcontext
getcontext().prec = 18
import numpy as np
from time import sleep

def clean(text):
    while text.startswith(" "):
        text = text[1:]
    while text.endswith(" "):
        text = text[:-1]
    return text

def escape(text):
    return eval(text)

def typer(value, type_):
    if not type_:
        return value
    match type_:
        case "int":
            to_ret = int(value)
        case "float":
            to_ret = float(value)
        case "dec":
            to_ret = Decimal(str(value))
        case "bool":
            to_ret = bool(value)
        case "bytes":
            to_ret = bytes(value)
        case "str":
            to_ret = str(value)

    return to_ret

def unpack(text):
    text = clean(text)
    types = ["int","float","dec","bool","bytes","str"]
    if True in [text.startswith(i+".") for i in types]:
        forced_type = types[[text.startswith(i+".") for i in types].index(True)]
        text = text.split(".",1)[1]
    else:
        forced_type = False
    integer = set("0987654321")
    floating = set("0987654321.")
    if text.startswith("[") and text.endswith("]"):
        my_list = []
        level = -1
        indexes = [1]
        for n, character in enumerate(text):
            if character in ["[","{"]:
                level+=1
            elif character in ["]","}"]:
                level-=1
            if level==0 and character==",":
                indexes.append(n+1)
        indexes.append(n+1)
        for m in range(1,len(indexes)):
            my_list.append(unpack(text[indexes[m-1]:indexes[m]-1]))
        return Variable(my_list, "const")
    elif text.startswith("{") and text.endswith("}"):
        dtype = {False: np.float64, "int": np.int64, "float": np.float64, "bool": np.bool, "bytes": np.ubyte}[forced_type]#fix this for all dtypes
        my_array = np.zeros([unpack(i) for i in text[1:-1].split(",")], dtype = dtype)
        return Variable(my_array,"const")
    elif set(text).issubset(floating) and not set(text).issubset(integer):
        return Variable(typer(float(text),forced_type),"const")
    elif set(text).issubset(integer):
        return Variable(typer(int(text),forced_type),"const")
    elif text.startswith("#"):
        return Variable(typer(int(text.split("#")[-1],16),forced_type),"const")
    elif text.startswith('"') and text.endswith('"'):
        return Variable(typer(escape(text),forced_type),"const")
    else:
        return Variable(text, "var")

def has_parens(text):
    if "(" in text:
        return True
    else:
        return False

def has_funs(text, funs):
    return True in [i+" " in text for i in funs]

def has_ops(text, ops):
    return True in [i in text for i in ops]

def is_assign(text):
    if "=" in text and not text.split("=",1)[1].startswith("="):
        return True

def linearize(text, varcount = 0):
    text = clean(text)
    ops = ["'","^","*","//","/","%","+","-","==","&&","!=",">","<","||"]
    special = ["++","--"]
    funs = ["print", "len", "goto", "input", "sub", "convert", "append", "warp"]
    actions = []

    if is_assign(text):
        target, text = [clean(i) for i in text.split("=",1)]
        if "," in target:
            target = [clean(i) for i in target.split(",")]

            start = 0
            end = start
            level = 0
            parts = []
            while end<len(text):
                if text[end] in ["[","{"]:
                    level+=1
                elif text[end] in ["]","}"]:
                    level-=1
                if text[end]=="," and level==0:
                    parts.append(clean(text[start:end]))
                    start = end+1
                end+=1
            parts.append(clean(text[start:end]))
            for n, part in enumerate(parts):
                to_ext, varlet = linearize(part, varcount)
                varcount+=varlet
                actions.extend(to_ext)
                actions.append(
                Action([target[n]], "=", [Variable(f"__var__{varcount-1}", "var")])
                )
                #varcount+=1
            text = target[-1]
            
        else:
            target = [target]
    else:
        target = None
    while has_parens(text) or has_funs(text, funs) or has_ops(text, ops):
        if has_parens(text):
            end = text.find(")")
            start = len(text[:end].rsplit("(",1)[0])
            a, varcount = linearize(text[start+1:end])
            text = text[:start]+f"__var__{varcount-1}"+text[end+1:]
            actions.extend(a)
        elif has_funs(text,funs):
            fun = funs[[text.rfind(fun) for fun in funs].index(max([text.rfind(fun) for fun in funs]))]#funs[[i+" " in text for i in funs].index(True)]
            start = text.rfind(fun)+len(fun)
            end = start
            level = 0
            parts = []
            while end<len(text):
                if text[end] in ["[","{"]:
                    level+=1
                elif text[end] in ["]","}"]:
                    level-=1
                if text[end]=="," and level==0:
                    parts.append(clean(text[start:end]))
                    start = end+1
                end+=1
            parts.append(clean(text[start:end]))
            variables = [unpack(i) for i in parts]
            actions.append(
                Action([f"__var__{varcount}"],fun, variables[:])
                )
            start = text.rfind(fun)+len(fun)###
            text = text[:start-len(fun)]+f"__var__{varcount}"
            #print("FUN", text)
            varcount+=1
        elif has_ops(text,ops):
            while has_ops(text,ops):
                line_list = [""]
                n = 0
                while n<len(text):
                    if 0 in [text[n:].find(a) for a in ops]:
                        action = ops[[text[n:].find(a) for a in ops].index(0)]
                        if line_list[0]=="" and action=="-":
                            line_list[-1]+="0"
                            line_list.append(action)
                        else:
                            line_list.append(action)
                        line_list.append("")
                        n+=len(action)
                    else:
                        line_list[-1]+=text[n]
                        n+=1

                #print(line_list)
                position = [line_list.index(a) for a in ops if a in line_list][0]

                if clean(line_list[position+1])=="":
                    one = unpack(clean(line_list[position-1]))
                    if clean(line_list[position+2])=="+":
                        actions.append(Action([clean(line_list[position-1])],"+",[one,unpack("1")]))
                        actions.append(Action([f"__var__{varcount}"],"=",[one]))
                    else:
                        actions.append(Action([clean(line_list[position-1])],"-",[one,unpack("1")]))
                        actions.append(Action([f"__var__{varcount}"],"=",[one]))
                    line_list.pop(position+1)
                else:
                    one, two = unpack(clean(line_list[position-1])), unpack(clean(line_list[position+1]))#action = line_list[position]
                    actions.append(Action([f"__var__{varcount}"],line_list[position],[one,two]))
                
                line_list[position-1:position+2] = [f"__var__{varcount}"]
                varcount+=1
                text = "".join(line_list)
                    
    if not target is None and len(target)==1:
        if varcount>0:
            actions.append(Action(target,"=",[Variable(f"__var__{varcount-1}","var")]))
        else:
            actions.append(Action(target,"=",[unpack(text)]))
    if not actions and text:
        actions.append(Action([f"__var__{varcount}"],"=",[unpack(text)]))
    return actions, varcount

class Action:
    def __init__(self, targets, type_, variables):
        self.targets = targets
        self.type = type_
        self.variables = variables

    def __str__(self):
        return f"{', '.join(self.targets)} <-[{self.type}]- {' '.join([str(i) for i in self.variables])}"

    def __repr__(self):
        return self.__str__()

    def do(self, d):
        vari = [i.access(d) for i in self.variables]
        if self.type=="=":#assignment
            if len(self.targets)>1:
                for targ in self.targets:
                    d[targ] = vari.pop(0)
            else:
                d[self.targets[0]] = vari[0]
        elif self.type=="'":
            d[self.targets[0]] = vari[0][vari[1]]
        elif self.type=="+":
            d[self.targets[0]] = vari[0]+vari[1]
        elif self.type=="-":
            d[self.targets[0]] = vari[0]-vari[1]
        elif self.type=="//":
            d[self.targets[0]] = vari[0]//vari[1]
        elif self.type=="/":
            if type(vari[0])!=str:
                d[self.targets[0]] = vari[0]/vari[1]
            else:
                for var in vari[1]:
                    vari[0] = vari[0].replace(r"\i", str(var), 1)
                d[self.targets[0]] = vari[0]
        elif self.type=="%":
            d[self.targets[0]] = vari[0]%vari[1]
        elif self.type=="*":
            d[self.targets[0]] = vari[0]*vari[1]
        elif self.type=="^":
            d[self.targets[0]] = vari[0]**vari[1]
        elif self.type=="<":
            d[self.targets[0]] = vari[0]<vari[1]
        elif self.type==">":
            d[self.targets[0]] = vari[0]>vari[1]
        elif self.type=="==":
            d[self.targets[0]] = vari[0]==vari[1]
        elif self.type=="!=":
            d[self.targets[0]] = vari[0]!=vari[1]
        elif self.type=="&&":
            d[self.targets[0]] = vari[0] and vari[1]
        elif self.type=="||":
            d[self.targets[0]] = vari[0] or vari[1]
        elif self.type=="len":
            d[self.targets[0]] = len(vari[0])
        elif self.type=="sub":
            temp_ = vari[0]
            match len(vari[2:]):
                case 1:
                    temp_[vari[2]] = vari[1]
                case 2:
                    temp_[vari[2]:vari[3]] = vari[1]
                case 3:
                    temp_[vari[2]:vari[3]:vari[4]] = vari[1]
            d[self.targets[0]] = temp_
        elif self.type=="convert":
            d[self.targets[0]] = type(vari[1])(vari[0])
        elif self.type=="append":
            vari[0].append(vari[1])
            d[self.targets[0]] = vari[0]
        elif self.type=="warp":
            d["__warp__"] = d["__warp__"]+[vari[0]]
        elif self.type=="print":
            d["__print__"] = d["__print__"]+[str(v) for v in vari]
        elif self.type=="input":
            while d["__input__in"]:
                sleep(0.01)
            d["__input__in"] = str(vari[0])
            while not d["__input__out"]:
                sleep(0.01)
            out = d["__input__out"]
            d["__input__in"] = ""
            d[self.targets[0]] = out
            
        elif self.type=="goto":
            d[f"goto_{__name__}"] = vari[0]

class MainAction:
    def __init__(self, line):
        if "$" in line:
            line, _ = line.split("$", 1)
        number, line = line.split(" ",1)
        if "." in number:
            number, self.alias = number.split(".")
        else:
            self.alias = None
        self.index = int(number, 16)
        if "?" in line:
            self.condition, line = [clean(i) for i in line.split("?")]
        else:
            self.condition = ""
        if ":" in line:
            line, self.alternative = [clean(i) for i in line.split(":")]
        else:
            self.alternative = ""

        self.expression = line
        self.cond = []
        for condlet in self.condition.split(";"):
            self.cond.extend(linearize(condlet)[0])
        self.expr = []
        for exprlet in self.expression.split(";"):
            self.expr.extend(linearize(exprlet)[0])
        self.alt = []
        for altlet in self.alternative.split(";"):
            self.alt.extend(linearize(altlet)[0])

    def run(self, d):
        if self.cond:
            for n in range(len(self.cond)):
                self.cond[n].do(d)
        #print("cond",d.get(self.cond[-1].targets[0]))
        if not self.cond or d.get(self.cond[-1].targets[0]):
            for n in range(len(self.expr)):
                self.expr[n].do(d)
        elif not d.get(self.cond[-1].targets[0]) and self.alt:
            for n in range(len(self.alt)):
                self.alt[n].do(d)

    def __str__(self):
        to_return = f"{hex(self.index)}"
        if self.cond:
            to_return+="\n    condition:\n        "+f"{(chr(10)+'        ').join([str(i) for i in self.cond])}"
        if self.expr:
            to_return+="\n    expression:\n        "+f"{(chr(10)+'        ').join([str(i) for i in self.expr])}"
        if self.cond:
            to_return+="\n    alternative:\n        "+f"{(chr(10)+'        ').join([str(i) for i in self.alt])}"
        return to_return
    
"""{hex(self.index)}
    condition:
        {(chr(10)+'        ').join([str(i) for i in self.cond])}
    expression:
        {(chr(10)+'        ').join([str(i) for i in self.expr])}
    alternative:
        {(chr(10)+'        ').join([str(i) for i in self.alt])}"""

class Variable:
    def __init__(self, value, type_):
        types = ["int","float","dec","bool","bytes","str"]#plus arrays and lists
        self.type = type_
        self.value = value

    def access(self, variables):
        if self.type=="var":
            v = variables.get(self.value)
            return v if not v is None else 0
        else:
            if isinstance(self.value, list):
                return [i.access(variables) for i in self.value]
            else:
                return self.value

    def __str__(self):
        return f"{self.type} {self.value if not isinstance(self.value, list) else [str(i) for i in self.value]}"

class Interpreter:
    def __init__(self, code):
        strings = [eval(f'"{i}"') for i in code.split('"')[1::2]]
        newcode = '"{}"'.join(code.split('"')[::2]).format(*["{"+f"__str__{i}"+"}" for i in range(len(strings))])
        self.actions = [MainAction(line) for line in newcode.split("\n") if line]
        self.indexes = [act.index for act in self.actions]
        self.aliases = [act.alias for act in self.actions]
        self.strings = dict()
        for i in range(len(strings)):
            self.strings[f"__str__{i}"] = strings[i]

        #expr desanitizer
        for n in range(len(self.actions)):
            for m in range(len(self.actions[n].expr)):
                for v in range(len(self.actions[n].expr[m].variables)):
                    if self.actions[n].expr[m].variables[v].type=="const" and isinstance(self.actions[n].expr[m].variables[v].value,str):
                        self.actions[n].expr[m].variables[v].value = self.actions[n].expr[m].variables[v].value.format(**self.strings)

        for n in range(len(self.actions)):
            for m in range(len(self.actions[n].cond)):
                for v in range(len(self.actions[n].cond[m].variables)):
                    if self.actions[n].cond[m].variables[v].type=="const" and isinstance(self.actions[n].cond[m].variables[v].value,str):
                        self.actions[n].cond[m].variables[v].value = self.actions[n].cond[m].variables[v].value.format(**self.strings)

        for n in range(len(self.actions)):
            for m in range(len(self.actions[n].alt)):
                for v in range(len(self.actions[n].alt[m].variables)):
                    if self.actions[n].alt[m].variables[v].type=="const" and isinstance(self.actions[n].alt[m].variables[v].value,str):
                        self.actions[n].alt[m].variables[v].value = self.actions[n].alt[m].variables[v].value.format(**self.strings)


    def __str__(self):
        lines = []
        for act in self.actions:
            lines.append(str(act))
        return "\n".join(lines)

    def run(self, d, start = None):
        d[f"goto_{__name__}"] = -1
        focus = 0 if start is None else self.indexes.index(start)
        while d[f"goto_{__name__}"]!=0 and focus<len(self.actions):
            self.actions[focus].run(d)
            if d[f"goto_{__name__}"]!=-1:
                if d[f"goto_{__name__}"]==0:
                    break
                if isinstance(d[f"goto_{__name__}"], int):
                    focus = self.indexes.index(d[f"goto_{__name__}"])
                else:
                    focus = self.aliases.index(d[f"goto_{__name__}"])
                d[f"goto_{__name__}"] = -1
            else:
                focus+=1

def worker(code, d, start = None):
    inter = Interpreter(code)
    inter.run(d, start)

def demo():
    a = unpack("[1,2,3,abc,[0]]")
    print(a)
    print(a.access({"abc":4}))
    acts, n = linearize("a = (33-1)+55/5^2")#(55//5)*8+3/9
    print(acts)
    d = dict()
    for act in acts:
        act.do(d)
    print(d["a"])

if __name__=="__main__":
    t = '''10 my_list = [1,2,4]
20 index = 0-1
30 my_list = sub my_list, 3, index
40 print my_list'''
    inter = Interpreter(t)
    print(inter)
    print(linearize('a = "Hello World!"'))
